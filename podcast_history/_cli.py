# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import argparse

from . import config
from . import tools
from . import sync
from . import gen
from . import opml


def identifier(s):
    s2 = config.slugify(s)
    if s != s2:
        raise ValueError
    return s


def parseargs():
    parser = argparse.ArgumentParser(
        description="A tool to follow podcast with all the history."
    )
    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument(
        "-c",
        "--config",
        dest="config",
        default=None,
        help="Configuration file. (Default: $XDG_CONFIG_DIR/podcast_history.cfg).",
    )

    subparsers = parser.add_subparsers(
        dest="subcommand", title="subcommands", required=True
    )

    parser_create_config = subparsers.add_parser(
        "create-config",
        parents=(common_parser,),
        help="Create the configuration file.",
        description="Create the configuration file, the configuration file must be edited after creation.",
    )

    parser_sync = subparsers.add_parser(
        "sync", parents=(common_parser,), help="Sync all active podcasts."
    )
    parser_sync.add_argument(
        "-v", "--verbose", action="store_true", help="Verbose mode"
    )

    parser_list = subparsers.add_parser(
        "list", parents=(common_parser,), help="List all podcasts."
    )
    parser_list.add_argument(
        "-d", "--details", action="store_true", help="Show details"
    )

    parser_show = subparsers.add_parser(
        "show", parents=(common_parser,), help="Show a podcast."
    )
    parser_show.add_argument("identifier", type=identifier)
    parser_show.add_argument(
        "-d", "--details", action="store_true", help="Show details"
    )

    parser_add = subparsers.add_parser(
        "add", parents=(common_parser,), help="Add a new podcast."
    )
    parser_add.add_argument("identifier", type=identifier)
    parser_add.add_argument("url", type=str)

    parser_del = subparsers.add_parser(
        "delete", parents=(common_parser,), help="Delete a podcast."
    )
    parser_del.add_argument("identifier", type=identifier)

    parser_activate = subparsers.add_parser(
        "activate", parents=(common_parser,), help="Activate a podcast."
    )
    parser_activate.add_argument("identifier", type=identifier)

    parser_inactivate = subparsers.add_parser(
        "inactivate", parents=(common_parser,), help="Inactivate a podcast."
    )
    parser_inactivate.add_argument("identifier", type=identifier)

    args = parser.parse_args()
    return args


def main():
    args = parseargs()

    if args.subcommand == "create-config":
        config.create_config_file(args.config)
    else:
        conf = config.get_config(args.config)
        if args.subcommand == "list":
            tools.list(conf, args.details)
        elif args.subcommand == "show":
            tools.show(conf, args.identifier, args.details)
        elif args.subcommand == "add":
            tools.add(conf, args.identifier, args.url)
        elif args.subcommand == "delete":
            tools.delete(conf, args.identifier)
        elif args.subcommand == "activate":
            tools.activation(conf, args.identifier, True)
        elif args.subcommand == "inactivate":
            tools.activation(conf, args.identifier, False)
        elif args.subcommand == "sync":
            if args.verbose:
                conf.verbose = True
            sync.sync_all(conf)
            gen.gen_all(conf)
            opml.gen_opml(conf)
