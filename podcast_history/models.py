# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import re
import enum
import unicodedata

from sqlalchemy import create_engine
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    ForeignKeyConstraint,
    Index,
    Integer,
    LargeBinary,
    UniqueConstraint,
    Text,
)
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, deferred
from sqlalchemy_utils import ChoiceType, URLType
from sqlalchemy.orm import Session


def get_db(db_string):
    if db_string is None:
        db_string = "sqlite:////tmp/test.db"
    return create_engine(db_string)


base = declarative_base()


class Feed(base):
    __tablename__ = "feeds"
    idname = Column(Text, primary_key=True)
    active = Column(Boolean)
    last_sync = Column(DateTime)
    url = Column(URLType)
    title = Column(Text)
    image = Column(Text)


class Entry(base):
    __tablename__ = "entries"
    feed_idname = Column(Text, ForeignKey("feeds.idname"), nullable=False, index=True)
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, index=True)
    title = Column(Text)
    summary = Column(Text)
    enclosure = Column(Text)
    enclosure_length = Column(Integer)
    enclosure_type = Column(Text)
    image = Column(Text)
    published = Column(Text)
    published_dt = Column(DateTime)
