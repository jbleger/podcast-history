# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import re
import unicodedata
import os
import sys
from dataclasses import dataclass
import configobj


def slugify(string):
    return re.sub(
        r"[-\s]+",
        "-",
        re.sub(
            r"[^a-z0-9]",
            "-",
            unicodedata.normalize("NFKD", string)
            .encode("ascii", "ignore")
            .decode()
            .lower(),
        ).strip(),
    )


@dataclass
class Config:
    dbfile: str
    dirprefix: str
    baseurl: str
    verbose: bool


def _get_config_file(configobjfile):
    if configobjfile is None:
        _home = os.path.expanduser("~")
        xdg_config_home = os.environ.get("XDG_CONFIG_HOME") or os.path.join(
            _home, ".config"
        )
        configobjfile = os.path.join(xdg_config_home, "podcast_history.cfg")
    return configobjfile


def create_config_file(config_file):
    configobjfile = _get_config_file(config_file)
    sample_config = {
        "localdir": "define here the directory where local files are stored",
        "baseurl": "define here the base url where the localdir is accessible",
        "dbfile": "define here the database file used to store metadata. e.g. ~/.podcast_history.db",
    }
    if not os.path.exists(configobjfile):
        co = configobj.ConfigObj(configobjfile)
        for k, v in sample_config.items():
            co[k] = v
        co.write()
        print(f"config file {configobjfile!r} was created. Please edit it.")
    else:
        print(
            f"config file {configobjfile!r} already exist. Delete the file before create a new config file.",
            file=sys.stderr,
        )
        sys.exit(1)


def get_config(config_file):
    configobjfile = _get_config_file(config_file)
    if not os.path.exists(configobjfile):
        print(f"config file {configobjfile!r} not found.", file=sys.stderr)
        sys.exit(1)
    co = configobj.ConfigObj(configobjfile)
    for k in ("localdir", "baseurl", "dbfile"):
        if k not in co:
            print(
                f"Invalid config file {configobjfile!r}, key {k!r} missing.",
                file=sys.stderr,
            )
            sys.exit(1)
        if " " in co[k]:
            print(
                f"Invalid config file {configobjfile!r}, {k} contains space.",
                file=sys.stderr,
            )
            print("Did you forget edit the config file?", file=sys.stderr)
            sys.exit(1)
        baseurl = co["baseurl"]
        while baseurl.endswith("/"):
            baseurl = baseurl[:-1]
        localdir = os.path.expandvars(os.path.expanduser(co["localdir"]))
        while localdir.endswith("/"):
            localdir = localdir[:-1]
        dbfile = "sqlite:///" + os.path.expandvars(os.path.expanduser(co["dbfile"]))
        try:
            os.mkdir(localdir)
        except FileExistsError:
            pass
        return Config(dbfile, localdir, baseurl, False)
