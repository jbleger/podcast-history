# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import time
import shutil

from . import models
from . import config


def list(conf: config.Config, details):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)

    with models.Session(db) as session:
        for feed in session.query(models.Feed).all():
            if details:
                entries = (
                    session.query(models.Entry)
                    .filter(models.Entry.feed_idname == feed.idname)
                    .all()
                )
                print(f"{feed.idname}:")
                print(f" - title: {feed.title}")
                print(f" - local: {os.path.join(conf.dirprefix,feed.idname+'.xml')}")
                print(f" - href: {conf.baseurl}/{feed.idname}.xml")
                print(f" - entries: {len(entries)}")
            else:
                print(
                    f"{feed.idname}: {'active' if feed.active else 'inactive'}, {feed.title}"
                )


def show(conf: config.Config, idname, details):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)

    with models.Session(db) as session:
        feed = session.query(models.Feed).filter(models.Feed.idname == idname).first()
        if feed is None:
            print(f"{idname} not found", file=sys.stderr)
            sys.exit(1)

        print(f"{idname}:")
        print(f" - title: {feed.title}")
        print(f" - local: {os.path.join(conf.dirprefix,feed.idname+'.xml')}")
        print(f" - href: {conf.baseurl}/{feed.idname}.xml")

        entries = (
            session.query(models.Entry)
            .filter(models.Entry.feed_idname == idname)
            .order_by(models.Entry.published_dt)
            .all()
        )
        print(f" - entries: # {len(entries)}")
        if details:
            for entry in entries:
                print(f" - {entry.published_dt}")
                print(f"   {entry.title}")
                print(
                    f"   {os.path.join(conf.dirprefix,feed.idname,entry.uuid,entry.enclosure)}"
                )


def add(conf: config.Config, idname, url):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)

    with models.Session(db) as session:
        feed = session.query(models.Feed).filter(models.Feed.idname == idname).first()
        if feed is not None:
            print(f"Identifier {idname!r} already exists.", file=sys.stderr)
            sys.exit(1)

        feed = models.Feed(idname=idname, url=url, active=True)
        session.add(feed)
        session.commit()


def delete(conf: config.Config, idname):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)

    with models.Session(db) as session:
        feed = session.query(models.Feed).filter(models.Feed.idname == idname).first()
        if feed is None:
            print(f"Identifier {idname!r} not found.", file=sys.stderr)
            sys.exit(1)
        print("Selected feed:")
        print(f" - idname: {idname}")
        print(f" - title: {feed.title}")
        print(
            "WARNING: This operation can not be undone. You can interrupt the process by hitting CTRL-C"
        )
        try:
            for r in range(5, 0, -1):
                print(f"{r}... ", end="")
                sys.stdout.flush()
                time.sleep(1)
            print()
        except KeyboardInterrupt:
            print("\nNothing done")
            return None
        session.query(models.Entry).filter(models.Entry.feed_idname == idname).delete()
        session.query(models.Feed).filter(models.Feed.idname == idname).delete()
        shutil.rmtree(os.path.join(conf.dirprefix, idname), ignore_errors=True)
        print("Deleted")
        session.commit()


def activation(conf: config.Config, idname, active):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)

    with models.Session(db) as session:
        feed = session.query(models.Feed).filter(models.Feed.idname == idname).first()
        if feed is None:
            print(f"Identifier {idname!r} not found.", file=sys.stderr)
            sys.exit(1)

        feed.active = active
        session.commit()
