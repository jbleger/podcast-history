# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import feedgen.feed

from . import models
from . import config


def gen_all(conf: config.Config):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)
    with models.Session(db) as session:
        feeds = session.query(models.Feed).all()
        for feed in feeds:
            gen_feed(conf, session, feed)


def gen_feed(conf: config.Config, session: models.Session, feed: models.Feed):
    if conf.verbose:
        print(f"Gen {feed.idname}")

    fg = feedgen.feed.FeedGenerator()
    fg.load_extension("podcast")
    fg.title(feed.title)
    if feed.image is not None:
        fg.logo(f"{conf.baseurl}/{feed.idname}/image/{feed.image}")
    fg.link(href=f"{conf.baseurl}/{feed.idname}.xml", rel="self")
    fg.description(feed.title)

    entries = (
        session.query(models.Entry)
        .filter(models.Entry.feed_idname == feed.idname)
        .order_by(models.Entry.published_dt)
        .all()
    )

    for entry in entries:
        fe = fg.add_entry()
        fe.id(entry.uuid)
        fe.title(entry.title)
        fe.enclosure(
            f"{conf.baseurl}/{feed.idname}/{entry.uuid}/{entry.enclosure}",
            str(entry.enclosure_length),
            entry.enclosure_type,
        )
        fe.summary(entry.summary)
        fe.published(entry.published)

    fg.rss_file(os.path.join(conf.dirprefix, f"{feed.idname}.xml"), pretty=True)
