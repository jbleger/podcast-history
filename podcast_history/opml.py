# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import time
import shutil

from . import models
from . import config


def _gen_opml(conf: config.Config, session, only_active):
    out = '<?xml version="1.0" encoding="UTF-8"?>\n'
    out += "<opml>\n"
    out += "<body>\n"
    for feed in session.query(models.Feed).all():
        href = f"{conf.baseurl}/{feed.idname}.xml"
        if feed.active or not only_active:
            out += f'<outline text={feed.title!r} type="rss" xmlUrl={href!r} />\n'
    out += "</body>\n"
    out += "</opml>\n"
    return out


def gen_opml(conf: config.Config):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)
    with models.Session(db) as session:
        with open(
            os.path.join(conf.dirprefix, "all-active.opml"), "w", encoding="utf8"
        ) as f:
            f.write(_gen_opml(conf, session, True))
        with open(os.path.join(conf.dirprefix, "all.opml"), "w", encoding="utf8") as f:
            f.write(_gen_opml(conf, session, False))
