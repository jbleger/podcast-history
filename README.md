
```
pip install --upgrade --index-url https://gitlab.com/api/v4/projects/42999517/packages/pypi/simple podcast-history
```

 - create config
   ```
   podcast-history create-config
   ```
 - edit config
 - add podcast
   ```
   podcast-history add IDENTIFIER URL
   ```
 - sync
   ```
   podcast-history sync -v
   ```
 - examine
   ```
   podcast-history list
   podcast-history show IDENTIFIER
   ```

# Bonus

Create a timer for automatic sync

 - create the file `~/.config/systemd/user/podcast-history.service`. **Last
   line:** Replace `HOME` by your `HOMEDIR` or change the path to have the executable.
    ```
    [Unit]
    Description=Podcast History

    [Service]
    Type=oneshot
    ExecStart=HOME/.local/bin/podcast-history sync -v
    ```

 - Create the timer, with file `.config/systemd/user/podcast-history.timer` with
   the content:
    ```
    [Unit]
    Description=Podcast History

    [Timer]
    OnBootSec=2m
    OnUnitActiveSec=15m
    Unit=podcast-history.service

    [Install]
    WantedBy=timers.target
    ```

 - Update the daemon by running:
    ```
    systemctl --user daemon-reload
    ```

 - Activate the timer:
    ```
    systemctl --user enable --now podcast-history.timer
    ```
