# Copyright 2023, Jean-Benoist Leger <jb@leger.tf>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import datetime
import shutil
import pathlib
import tempfile

import feedparser
import requests
import yt_dlp

from . import models
from . import config
from .config import slugify


def _yt_dl_mp3(url, verbose=True):
    data = None
    with tempfile.TemporaryDirectory() as tmpdir:
        ydl_opts = {
            "format": "m4a/bestaudio/best",
            "noprogress": True,
            "postprocessors": [
                {
                    "key": "FFmpegExtractAudio",
                    "preferredcodec": "mp3",
                }
            ],
            "paths": {"home": tmpdir, "temp": tmpdir},
        }
        if not verbose:
            ydl_opts["quiet"] = True
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            error_code = ydl.download(url)
        if error_code == 0:
            f = next((f for f in os.listdir(tmpdir) if f.endswith(".mp3")), None)
            if f is not None:
                with open(pathlib.Path(tmpdir) / f, "rb") as mp3:
                    data = mp3.read()
    return data


def sync_all(conf: config.Config):
    db = models.get_db(conf.dbfile)
    models.base.metadata.create_all(db)
    with models.Session(db) as session:
        feeds = session.query(models.Feed).filter(models.Feed.active).all()
        for feed in feeds:
            sync_feed(conf, session, feed)


def sync_feed(conf: config.Config, session: models.Session, feed: models.Feed):
    if conf.verbose:
        print(f"Sync {feed.idname}")

    try:
        os.mkdir(os.path.join(conf.dirprefix, feed.idname))
    except FileExistsError:
        pass
    parsed = feedparser.parse(feed.url)
    feed.last_sync = datetime.datetime.now()
    feed.title = parsed["feed"]["title"]
    try:
        r = requests.get(parsed["feed"]["image"]["href"], timeout=30)
        if r.ok:
            imagename = parsed["feed"]["image"]["href"].split("/")[-1]
            try:
                os.mkdir(os.path.join(conf.dirprefix, feed.idname, "image"))
            except FileExistsError:
                pass
            with open(
                os.path.join(conf.dirprefix, feed.idname, "image", imagename), "wb"
            ) as f:
                f.write(r.content)

            feed.image = imagename
    except KeyError:
        pass
    for fentry in parsed["entries"]:
        if "rf_redirect" in fentry["id"]:
            continue

        uuid = slugify(fentry["id"])
        if (
            session.query(models.Entry)
            .filter(models.Entry.feed_idname == feed.idname)
            .filter(models.Entry.uuid == uuid)
            .first()
        ) is not None:
            continue

        if conf.verbose:
            print("    -> found new")
        entrydir = os.path.join(conf.dirprefix, feed.idname, uuid)
        try:
            os.mkdir(entrydir)
        except FileExistsError:
            shutil.rmtree(entrydir)
            os.mkdir(entrydir)

        if "links" not in fentry:
            continue

        enclosures = [x for x in fentry["links"] if x["rel"] == "enclosure"]
        alternates = [x for x in fentry["links"] if x["rel"] == "alternate"]

        if enclosures:
            if "href" not in enclosures[0]:
                continue

            r = requests.get(enclosures[0]["href"], timeout=30)
            if not r.ok:
                continue
            enclosure = enclosures[0]["href"].split("/")[-1]
            enclosure_length = (
                enclosures[0]["length"] if "length" in enclosures[0] else None
            )
            enclosure_type = enclosures[0]["type"] if "type" in enclosures[0] else None
            with open(os.path.join(entrydir, enclosure), "wb") as f:
                f.write(r.content)
        elif alternates:
            if "href" not in alternates[0]:
                continue
            if "https://www.youtube.com" in alternates[0]["href"]:
                data = _yt_dl_mp3(alternates[0]["href"], conf.verbose)
                if data is None:
                    continue
                enclosure = uuid + ".mp3"
                enclosure_length = len(data)
                enclosure_type = "audio/mpeg"
                with open(os.path.join(entrydir, enclosure), "wb") as f:
                    f.write(data)
            else:
                continue
        else:
            continue

        image = None
        if "image" in fentry and "href" in fentry["image"]:
            r = requests.get(fentry["image"]["href"], timeout=30)
            if r.ok:
                image = fentry["image"]["href"].split("/")[-1]
                with open(os.path.join(entrydir, image), "wb") as f:
                    f.write(r.content)

        pub = fentry["published_parsed"]
        entry = models.Entry(
            feed_idname=feed.idname,
            uuid=uuid,
            title=fentry["title"],
            summary=fentry["summary"],
            enclosure=enclosure,
            enclosure_length=enclosure_length,
            enclosure_type=enclosure_type,
            image=image,
            published=fentry["published"],
            published_dt=datetime.datetime(
                pub.tm_year, pub.tm_mon, pub.tm_mday, pub.tm_hour, pub.tm_min
            ),
        )
        session.add(entry)
        session.commit()
